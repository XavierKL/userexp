﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public Rigidbody2D bulletBody;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("AutoDestroy", 1f);
    }

    // Update is called once per frame

    void AutoDestroy(){
        
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D col){

        if (col.gameObject.tag == "Enemy"){

            Destroy(gameObject);
        }
    }
}
