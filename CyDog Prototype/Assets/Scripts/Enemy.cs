﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public Rigidbody2D body;
    Vector2 movement;
    float moveSpeed = 10f;
    public Transform player;
    
    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player").transform;
        body = this.GetComponent<Rigidbody2D>();

    }

    void Update(){

        Vector3 direction = player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        body.rotation = angle;
        direction.Normalize();
        movement = direction;
    }

    void FixedUpdate(){

        EnemyMove(movement);
    }

    void EnemyMove(Vector2 direction){

        body.MovePosition((Vector2)transform.position + (direction * moveSpeed * Time.fixedDeltaTime));
    }

    void OnTriggerEnter2D(Collider2D col){

        if (col.gameObject.tag == "Player"){

            movement = Vector2.zero;
            moveSpeed = 0;
        }

        if (col.gameObject.tag == "Projectile"){

            Destroy(gameObject);
        }
    }
}
