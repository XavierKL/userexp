﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    float movementSpeed = 5f;
    public Rigidbody2D body;
    public Rigidbody2D gunBody;
    public Camera cam;

    Vector2 movement;
    Vector2 mousePosition;

    // Update is called once per frame
    void Update()
    {
        
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
    }

    void FixedUpdate(){

        body.MovePosition(body.position + movement * movementSpeed * Time.fixedDeltaTime);
        gunBody.MovePosition(body.position + movement * movementSpeed * Time.fixedDeltaTime);


        Vector2 lookDirection = mousePosition - body.position;
        float angle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg - 90;
        gunBody.rotation = angle; 
    }
}
