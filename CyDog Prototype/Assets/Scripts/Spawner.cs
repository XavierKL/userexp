﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
   
    public Transform[] spawnPoints;
    public GameObject enemyPrefab;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSpawning());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator StartSpawning(){

        while (true){

            yield return new WaitForSeconds(1);

            int randomNumber = Random.Range(0, spawnPoints.Length);
            Instantiate(enemyPrefab, spawnPoints[randomNumber].transform.position, spawnPoints[randomNumber].transform.rotation);

            yield return new WaitForSeconds(1);
        }
    }
}
